
import React from 'react';
import Header from './components/Header';
import Content from './components/Content';
import Footer from './components/Footer';
import PageBasket from './components/PageBasket';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams
} from "react-router-dom";
import PageFavorites from './components/PageFavorites';


class App extends React.Component {
    constructor() {
        super();
        this.state = { data: [] };
        this.getProductById = this.getProductById.bind(this);
    };
    async componentDidMount() {
        const response = await fetch('./data.json');
        const json = await response.json();
        this.setState({ data: json });
    }
    getProductById(setnumber) {
        const product = this.state.data.find(
            element => String(element.setnumber) === String(setnumber)
        );
        return product;
    }
    render() {

        return (
            <Router>
                <div className="App">
                    <Switch>
                        <Route exact path='/' >
                            <Header getProductById={this.getProductById} />
                            <Content data={this.state.data} />
                            <Footer />
                        </Route>
                        <Route path='/basket' >
                            <PageBasket getProductById={this.getProductById} />
                        </Route>
                        <Route path='/favorites' >
                            <PageFavorites getProductById={this.getProductById} />
                        </Route>
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;
