import React, { useState, useEffect } from 'react';
import {
    BrowserRouter as Router,
    Link,
} from "react-router-dom";
import { TiArrowBack } from 'react-icons/ti';
import Menu from './Menu';
import MenuTop from './MenuTop';
import ProductForFavorites from './ProductForFavorites';


const PageFavorites = () => {

    let favorit = localStorage.getItem('favorites');
    if (!favorit) {
        favorit = [];
    }
    else {
        favorit = JSON.parse(localStorage.getItem('favorites'))
    }
    let key = 0;
    const [localFavorit, setLocalFavorit] = useState(favorit) //создали стейт локалФэйворит в нем хранится массив данных о продукте
    const [products, setProducts] = useState([]); // 
    useEffect(() => {
        setProducts(localFavorit.map(         // сказали, чтобы эти продукты определялись нашим стейтом, локалФэйворитом
            element => {
                const productItem = <ProductForFavorites
                    key={key++}
                    src={element.src}
                    title={element.title}
                    price={element.price}
                    text={element.text}
                    setnumber={element.setnumber}
                    setLocalStorage={setLocalFavorit} />;
                return productItem;
            }));
        }, [localFavorit]) // сказали useEffect-у выполняться при обновлении локалФэйворита
        return (
            <div>
                <MenuTop />
                <Menu />
                <div className="pageFavorites">
                    <Link exact to='/' style={{ color: "black" }}><TiArrowBack size={40} style={{ color: "orange" }} />BACK TO HOMEPAGE</Link>
                    <p style={{ fontSize: "30px", color: "orange" }}>Your favorite products</p>
                    <div>{products}</div>
                </div>

            </div >

        )
    }


export default PageFavorites;