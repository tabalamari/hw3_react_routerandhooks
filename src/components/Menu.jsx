import React from 'react';
import {
    BrowserRouter as Router,
    Link,
} from "react-router-dom";


const Menu = () => {

        return (
            <div className="menu-wrapper">
                <div className="menu">
                    <Link exact to='/'>
                    <div className="menu-logo">
                        <img className="logo-img" src="./img/Vector Smart Object@1X.png" alt="Here is a logo" />
                        <p className="logo-letter">M</p>
                        <p class="logo-name">Store</p>
                    </div>
                    </Link>
                    <div className="menu-list">
                        <Link exact to='/' style={{ color: "black" }}><p>Home</p></Link>
                        <p>cd's</p>
                        <p>About us</p>
                        <p>Contacts</p>
                    </div>
                </div>

            </div>
        )
    }


export default Menu;