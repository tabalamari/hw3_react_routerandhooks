import React from 'react';
import PropTypes from 'prop-types';

const Modal = (props) => {
    
    const addToCart = () => {
        let basket = localStorage.getItem('basket');
        if (!basket) {
            basket = [];
        }
        else {
            basket = JSON.parse(localStorage.getItem('basket'))
        }
        const prod = {};
        prod.setnumber = props.setnumber;
        prod.title = props.title;
        prod.text = props.text;
        prod.src = props.src;
        prod.price = props.price;

        basket.push(prod);

        const basketString = JSON.stringify(basket);
        localStorage.setItem('basket', basketString);
    }
    

        return (
            <div className={"modal_wrapper " + props.className}>
                <div className="modal">
                    <header className="modal_header" >
                        <span className="header_title">{props.title}</span>
                        <span className="icon_close" onClick={props.makeInvisible}>✕</span>
                    </header>
                    <div className="modal_content">
                        <p>{props.setnumber}</p>
                        <img src={props.src} />
                        <p>{props.text}</p>
                        <p>{props.price}</p>
                        <div className="modal_buttons">
                            <ModalButton onClick={props.makeInvisible} className="cancel_button" text="Cancel" />
                            <ModalButton onClick={addToCart} className="add_button" text="Add to cart" />
                        </div>
                    </div>
                </div>

            </div>
        )
    }


const ModalButton = (props) => {
            return (
            <div>
                <button onClick={props.onClick} className="modal-button">{props.text}</button>
            </div>
        )
    }

Modal.propTypes = {
    setnumber: PropTypes.number,
    className: PropTypes.string,
    title: PropTypes.string,
    makeInvisible: PropTypes.func,
    src: PropTypes.string,
    text: PropTypes.string,
    price: PropTypes.string,
    onClick: PropTypes.func
};

export default Modal;


