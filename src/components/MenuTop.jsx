import React from 'react';
import PropTypes from 'prop-types';
import { AiFillStar } from 'react-icons/ai';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams
} from "react-router-dom";


const CartButton = () => {
    
        return (
            <div>
                <button className="cart"><i className="fa fa-shopping-basket" aria-hidden="true"></i>
                        Cart
                </button>
            </div>
        )
    }



const MenuTop = (props) => {
    
        return (
            <div className="menu-top__wrapper">
                <div className="menu-top">
                    <div className="icons">
                        <a href="#"><i className="fa fa-facebook-square" aria-hidden="true"></i></a>
                        <a href="#"><i className="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#"><i className="fa fa-telegram" aria-hidden="true"></i></a>
                        <a href="#"><i className="fa fa-twitter-square" aria-hidden="true"></i></a>
                    </div>
                    <div className="menu-top__right">
                        <button className="login">Login</button>/<button className="register">Register</button>
                        <Link to='favorites'><AiFillStar size={18} style={{backgroundColor:"#CC3333", padding:"4px", margin:"3px 0 0 0", borderRadius:"5px" }} getProductById={props.getProductById} /></Link>
                        <Link to='basket'><CartButton getProductById={props.getProductById} /></Link>
                    </div>
                </div>
            </div>
        )
    }


MenuTop.propTypes = {
    productById: PropTypes.string,
    getProductById: PropTypes.func,
    className: PropTypes.string,
};

export default MenuTop;