import React from 'react';


const SubFooter = () => {

        return (
            <div className="sub-footer">
            <p className="footer-info">Home</p>
            <p className="footer-info">Portfolio</p>
            <p className="footer-info">Sitemap</p>
            <p className="footer-info">Contact</p>
        </div>
        )
    }


export default SubFooter;