import React, { useState } from 'react';
import { AiFillStar } from 'react-icons/ai';


const Star = (props) => {

    let thisStarIsFavorite = false;
    let stringFromLocalStorage = localStorage.getItem('favorites');
    
    let arrayFromString = JSON.parse(stringFromLocalStorage);
    if (!arrayFromString) {
        arrayFromString = [];
    }
    const functionForFind = (item) => {
        if (item.setnumber === props.setnumber) {
            return true;
        }
        else {
            return false;
        }
    }
    let foundElement = arrayFromString.find(functionForFind);
    
    thisStarIsFavorite = foundElement;
    const [isFavorite, setIsFavorite] = useState(thisStarIsFavorite);

    const setFavorite = () => {
        setIsFavorite(!isFavorite)
        let favorit = localStorage.getItem('favorites');
        if (!favorit) {
            favorit = [];
        }
        else {
            favorit = JSON.parse(localStorage.getItem('favorites'))
        }
        const prod = {};
        prod.setnumber = props.setnumber;
        prod.title = props.title;
        prod.text = props.text;
        prod.src = props.src;
        prod.price = props.price;

        favorit.push(prod);
        const basketString = JSON.stringify(favorit);
        localStorage.setItem('favorites', basketString);

    }
    const removeFromFavorites = () => {
        const favorit = JSON.parse(localStorage.getItem('favorites'));
        const favoritNew = favorit.filter((item) => item.setnumber !== props.setnumber);
        localStorage.setItem('favorites', JSON.stringify(favoritNew));
        props.setLocalStorage(JSON.parse(localStorage.getItem('favorites')));
    }

    return (
        <AiFillStar onClick={setFavorite} color={isFavorite ? "orange" : "black"} size={30} />
    )
}



export default Star;

