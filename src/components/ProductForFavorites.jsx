import React from 'react';
import Star from './Star';
import Images from './Images';


const ProductForFavorites = (props) => {
    return (
        <div>
            <div className="product-wrapper">
                <Star
                    title={props.title}
                    setnumber={props.setnumber}
                    price={props.price}
                    src={props.src}
                    text={props.text}
                    isFavorite={true}
                // isFavorite={localStorage.getItem('fav_' + props.setnumber) === 'yes'}
                />
                <Images src={props.src} />
                <p>{props.title}</p>
                <p>{props.text}</p>
                <div className="price">
                    <p>{props.price}</p>
                </div>
            </div>
        </div>
    )
}


export default ProductForFavorites;