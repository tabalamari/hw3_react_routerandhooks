import React from 'react';
import Images from './Images';


const ProductForBasket = (props) => {

    const removeFromBasket = () => {
        const basket = JSON.parse(localStorage.getItem('basket'));
        const basketNew = basket.filter((item) => item.setnumber !== props.setnumber);
        localStorage.setItem('basket', JSON.stringify(basketNew));
        props.setLocalStorage(JSON.parse(localStorage.getItem('basket')));
    }
    return (
        <div>
            <div className="product-wrapper">
                <div className="remove" onClick={removeFromBasket}></div>
                <Images src={props.src} />
                <p>{props.title}</p>
                <p>{props.text}</p>
                <div className="price">
                    <p>{props.price}</p>
                </div>
            </div>
        </div>
    )
}


export default ProductForBasket;