import React from 'react';
import MenuTop from './MenuTop';
import Menu from './Menu';
import Carousel from './Carousel';
import PropTypes from 'prop-types';



const Header = (props) => {
    
        return (
            <header>
                <MenuTop getProductById = {props.getProductById} />
                <Menu />
                <Carousel />
            </header>)
    }

Header.propTypes = {
    getProductById: PropTypes.func,
};

export default Header; 