import React from 'react';
import FooterInfo from './FooterInfo';
import SubFooter from './SubFooter';


const Footer = () => {
    
        return (
            <footer className="footer">
                <FooterInfo />
                <SubFooter />
            </footer>)
}
export default Footer;