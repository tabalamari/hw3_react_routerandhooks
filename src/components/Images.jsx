import React, { Component } from 'react'



const Images = (props) => {
    
        return (
            <img src={props.src}
                alt={props.alt}
            />)
    }

export default Images;