import React from 'react';


const Title = () => {

    return (
        <div className="content__title-block">
            <div className="title">
                <div className="title-text__above">*</div>
                <div className="title-text">Welcome to musica, check our latest albums</div>
                <div className="title-text__below">*</div>
                <div className="title-blocks">
                    <div className="check">
                        <div className="fa-style">
                            <i className="fa fa-heart" aria-hidden="true"></i>
                            <p>check our cd collection</p>
                        </div>
                        <p>Donec pede justo, fringilla vel, al, vulputate
                        eget, arcu. In enim justo.
                        </p>
                    </div>
                    <div className="listen">
                        <div className="fa-style">
                            <i className="fa fa-headphones" aria-hidden="true"></i>
                            <p>listen before purchase</p>
                        </div>
                        <p>Donec pede justo, fringilla vel, al, vulputate
                        eget, arcu. In enim justo.
                        </p>
                    </div>
                    <div className="events">
                        <div className="fa-style">
                            <i className="fa fa-calendar-check-o" aria-hidden="true"></i>
                            <p>upcoming events</p>
                        </div>
                        <p>Donec pede justo, fringilla vel, al, vulputate
                        eget, arcu. In enim justo.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Title;