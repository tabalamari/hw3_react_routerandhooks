import React, { useState, useEffect } from 'react';
import {
    BrowserRouter as Router,
    Link,
} from "react-router-dom";
import { TiArrowBack } from 'react-icons/ti';
import ProductForBasket from './ProductForBasket';
import MenuTop from './MenuTop';
import Menu from './Menu';

const PageBasket = () => {

    let basket = localStorage.getItem('basket');
    if (!basket) {
        basket = [];
    }
    else {
        basket = JSON.parse(localStorage.getItem('basket'))
    }
    let key = 0;
    const [localBasket, setLocalBasket] = useState(basket) //создали стейт локалБаскет в нем хранится массив данных о продукте
    const [products, setProducts] = useState([]); // 
    useEffect(() => {                      
        setProducts(localBasket.map(         // сказали, чтобы эти продукты определялись нашим стейтом, локалБаскетом
            element => {
                const productItem = <ProductForBasket
                    key={key++}
                    src={element.src}
                    title={element.title}
                    price={element.price}
                    text={element.text}
                    setnumber={element.setnumber}
                    setLocalStorage={setLocalBasket} // в сам PродактforBasket передаем функцию обновления в локалБаскете, обновление стейта
                    />;
                return productItem;
            }));
    }, [localBasket]) // сказали useEffect-у выполняться при обновлении локалБаскета


    return (
        <div>
            <MenuTop />
            <Menu />
            <div className="pageBasket">
                <Link exact to='/' style={{ color: "black" }}><TiArrowBack size={40} style={{ color: "#CC3333" }} />BACK TO HOMEPAGE</Link>
                <p style={{ fontSize: "30px", color: "#CC3333" }}>Products in your cart</p>
                <div>{products}</div>
            </div>

        </div >

    )
}


export default PageBasket;