import React, { useState} from 'react';
import Modal from './Modal';
import Star from './Star';
import Images from './Images';



const AddButton = (props) => {
    const [state, setState] = useState({
        visible: false
    })
    const makeVisible = () => {
        setState({
            visible: true
        });
    }
    const makeInvisible = () => {
        setState({
            visible: false
        });
    }
    return (
        <div>
            <button onClick={makeVisible} className="add">Add to cart</button>
            <Modal
                makeInvisible={makeInvisible}
                title={props.title}
                price={props.price}
                text={props.text}
                src={props.src}
                setnumber={props.setnumber}
                className={state.visible ? "flex" : "unvisible"} />

        </div>
    )
}


const Product = (props) => {
    return (
        <div>
            <div className="product-wrapper">
                <Star
                    title={props.title}
                    setnumber={props.setnumber}
                    price={props.price}
                    src={props.src}
                    text={props.text}

                    isFavorite={localStorage.getItem('fav_' + props.setnumber) === 'yes'}
                />
                <Images src={props.src} />
                <p>{props.title}</p>
                <p>{props.text}</p>
                <div className="price">
                    <p>{props.price}</p>
                    <AddButton title={props.title} price={props.price} text={props.text} src={props.src} setnumber={props.setnumber} />
                </div>
            </div>
        </div>
    )
}

export default Product;
