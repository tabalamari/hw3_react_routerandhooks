import React from 'react';
import Title from './Title';
import AlbumsList from './AlbumsList';
import PropTypes from 'prop-types';


const Content = (props) => {
    
        return (
            <main>
                <Title />
                <AlbumsList data={props.data} />
            </main>)
    }


Content.propTypes = {
    data: PropTypes.array,
    };

export default Content;