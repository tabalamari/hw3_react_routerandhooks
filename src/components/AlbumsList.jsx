import React from 'react';
import Product from './Product';
import PropTypes from 'prop-types';


const AlbumsList = (props) => {

    let key = 0;
    const products = props.data.map(
        element => {
            const productItem = <Product
                key={key++}
                src={element.src}
                title={element.title}
                price={element.price}
                text={element.text}
                setnumber={element.setnumber} />;
            return productItem;
        });
    return (
        <div class="albums-list">
            <p className="albums-list__title">latest arrivals in musica</p>
            <div className="albums">
                <div className="albums-list__product">
                    {products.map((item) => {
                        return item
                    })}
                </div>
            </div>

            <div className="publishers">
                <p className="albums-list__title">our most important publishers</p>

                <div className="publishers-list">
                    <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                    <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                    <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                    <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                    <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                    <div className="publishers-box"><img src="./img/man@1X.png" alt="image" /></div>
                </div>
            </div>
        </div >
    )
}


AlbumsList.propTypes = {
    setnumber: PropTypes.number,
    alt: PropTypes.string,
    isFavorite: PropTypes.func,
    data: PropTypes.array,
    header: PropTypes.string,
    src: PropTypes.string,
    text: PropTypes.string,
    price: PropTypes.string,
    title: PropTypes.string,
};

export default AlbumsList;
